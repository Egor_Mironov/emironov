package ua.step.homework;

/*
 * Шаблон для решения домашнеего задания 2. 
 */

/**
 * В переменной n хранится натуральное (целое) трехзначное число. Создайте
 * программу, вычисляющую и выводящую на экран сумму цифр числа n при любых
 * значения n.
 */
public class Task02
{
    public static void main(String[] args)
    {
        int n = 111;
        // строка кода ниже нужна для тестирования (смотри @see Task2Test.java)
        // не изменяйте и не удаляйте ее
        n = (args.length == 1) ? Integer.valueOf(args[0]) : n;
        // используй переменную с именем n в качестве входных данных
        int hun=n/100;
        int tens=n%100/10;
        int numb=n%10;
        System.out.println(hun+tens+numb);
    }
}